package com.portal.logger;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

public class AppLogger {

	private Logger logger;
	
	private AppLogger AppLogger;

	private String loggerName;

	public AppLogger(String logger) {
		this.loggerName = logger;
	}
	
	private String getLoggerName() {
		return this.loggerName;
	}

	private Logger getLogger() {
		if (logger == null) {
			synchronized (AppLogger.class) {
				if (logger == null) {					
					String loggerName = getLoggerName();
					System.out.println("logger ::: " + loggerName);
					//logger = LoggerFactory.getLogger();
					logger = LoggerFactory.getILoggerFactory().getLogger(loggerName);
				}
			}
		}
		return logger;
	}
	
	/**
	 * Method returns Exception StackTrace.
	 * 
	 * @param exception Exception
	 * @return
	 */
	private String getExceptionStackTrace(final Exception exception) {
		String sStackTrace = null;
		if (exception != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			((Exception) exception).printStackTrace(pw);
			sStackTrace = sw.toString();
		}
		return sStackTrace;
	}
	
	private String callingDetails() {
		StackTraceElement stackTraceElement = (Thread.currentThread().getStackTrace())[3];
		String callingClassName = stackTraceElement.getClassName();
		String callingMethodName = stackTraceElement.getMethodName();
		String message = callingClassName + " " + callingMethodName + ": [Thread: " + Thread.currentThread().getId()
				+ "]: ";
		return message;
	}
	
	public void trace(final Object logMessage) {
		Logger logger = getLogger();
		logger.trace(callingDetails() + logMessage);
	}

	public void debug(final Object logMessage) {
		Logger logger = getLogger();
		logger.debug(callingDetails() + logMessage);
	}

	public void info(final Object logMessage) {
		Logger logger = getLogger();
		logger.info(callingDetails() + logMessage);
	}

	public void warn(final Object logMessage) {
		Logger logger = getLogger();
		logger.warn(callingDetails() + logMessage);
	}

	public void error(final Object logMessage) {
		Logger logger = getLogger();
		logger.error(callingDetails() + logMessage);
	}
	
	public void error(final Object logMessage, final Exception error) {
		Logger logger = getLogger();
		String additionalDetails = "";
		logger.error(callingDetails() + logMessage  + " [ " + additionalDetails + "] \n" + getExceptionStackTrace(error));
	}
	/**
	 * Method used to log execution time of any process or method.
	 * 
	 * @param process String
	 * @return long
	 */
	public long logExecutionStartTime(String process) {
		debug("Execution started for - " + process);
		return System.currentTimeMillis();
	}

	/**
	 * Method used to log End time taken to execute a process or method.
	 * 
	 * @param process   String
	 * @param startTime long
	 */
	public void logExectionEndTime(String process, long startTime) {
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		double elapsedTimeInSec = elapsedTime / 1000.0;
		;
		debug("Execution ended for - " + process + " ::: " + elapsedTime + " miliseconds (" + elapsedTimeInSec
				+ " sec)");
	}

}
