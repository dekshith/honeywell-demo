package com.portal.controller;

import com.portal.exception.EmpException;
import com.portal.exception.NoSuchEmpException;
import com.portal.model.Employee;
import com.portal.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PutMapping(value = "/employee", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> addEmployee(@Valid @RequestBody Employee emp) throws EmpException {
        HttpStatus httpStatus = HttpStatus.CREATED;
        try {
            Integer empId = employeeService.addEmployee(emp);
            emp.setId(empId);
        } catch(Exception e) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<Employee>(emp, httpStatus);
    }

    @GetMapping(value = "/employee/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployee(@PathVariable("id") Integer empId) {
        HttpStatus httpStatus = HttpStatus.OK;
        Employee emp = null;
        try {
            emp = employeeService.getEmployee(empId);
        } catch (NoSuchEmpException e) {
            httpStatus = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<Employee>(emp, httpStatus);
    }

    @GetMapping(value = "/employees", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Employee> getEmployeesListData() {
        List<Employee> employeeList = employeeService.getEmployeeList();
        System.out.println("Employee List: " + employeeList);
        return employeeList;
    }

    @DeleteMapping(value = "/employee/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteEmployee(@PathVariable("id") Integer empId) throws NoSuchEmpException {
        employeeService.deleteEmployee(empId);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

}
