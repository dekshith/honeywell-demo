package com.portal.config;

import com.portal.logger.AppLogger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
@ComponentScan({ "com.portal.*" })
public class AppConfig implements WebMvcConfigurer {

    @Value("${logger}")
    private String loggerName;

    @Bean
    public AppLogger logger() {
        return new AppLogger(loggerName);
    }
}
