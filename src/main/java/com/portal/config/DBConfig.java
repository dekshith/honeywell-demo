package com.portal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DBConfig {

    @Bean(name = "empPortalDS")
    @ConfigurationProperties(prefix = "spring.datasource.emp-portal-ds")
    public DataSource empPortalDataSource() {
        return DataSourceBuilder.create().build();
    }
}
