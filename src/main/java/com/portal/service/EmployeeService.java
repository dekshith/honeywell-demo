package com.portal.service;

import com.portal.dao.EmployeeDAO;
import com.portal.entity.EmployeeEntity;
import com.portal.exception.EmpException;
import com.portal.exception.ErrorCode;
import com.portal.exception.NoSuchEmpException;
import com.portal.logger.AppLogger;
import com.portal.model.Employee;
import com.portal.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AppLogger logger;

    /**
     * Add an employee.
     * @param emp Employee
     * @return Integer
     * @throws EmpException
     */
    public Integer addEmployee(final Employee emp) throws EmpException {
        Integer empId;
        EmployeeEntity empEntity = new EmployeeEntity();
        empEntity.setName(emp.getName());
        empEntity.setAge(emp.getAge());
        try {
            empId = employeeDAO.addEmployeeData(empEntity);
        } catch (Exception e) {
            logger.error("Error occurred while adding employee details : " + emp, e);
            throw new EmpException("Unable to add employee to database", e, ErrorCode.DB_ERROR);
        }
        return empId;
    }

    /**
     * Returns employee data for given employee ID input
     * @param empId Integer
     * @return Employee
     * @throws NoSuchEmpException
     */
    public Employee getEmployee(final Integer empId) throws NoSuchEmpException {
        Employee emp = null;
        try {
            Optional<EmployeeEntity> optEmpEntity = employeeRepository.findById(empId);
            if (optEmpEntity.isPresent()) {
                EmployeeEntity empEntity = optEmpEntity.get();
                emp = new Employee();
                emp.setId(empEntity.getId());
                emp.setName(empEntity.getName());
                emp.setAge(empEntity.getAge());
            } else {
                logger.error("No such employee with ID:" + empId);
                throw new NoSuchEmpException("No such user", ErrorCode.NO_SUCH_EMPLOYEE);
            }
        } catch (NoSuchEmpException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error occurred while fetching employee details for ID : " + empId, e);
            throw e;
        }
        return emp;
    }

    /**
     * Returns list of existing employees.
     * @return List
     */
    public List<Employee> getEmployeeList() {
        Employee emp;
        List<Employee> empList = new ArrayList<>();
        try {
            List<EmployeeEntity> employeeEntityList = employeeRepository.findAll();
            if (employeeEntityList != null) {
                for (EmployeeEntity empEntity : employeeEntityList) {
                    emp = new Employee();
                    emp.setId(empEntity.getId());
                    emp.setName(empEntity.getName());
                    emp.setAge(empEntity.getAge());
                    empList.add(emp);
                }
            }
        } catch (Exception e) {
            logger.error("Error occurred while fetching all employee list", e);
            throw e;
        }
        return empList;
    }

    /**
     * Delete existing employee from table.
     * @param empId Integer
     * @throws NoSuchEmpException
     */
    public void deleteEmployee(final Integer empId) throws NoSuchEmpException {
        try {
            employeeDAO.deleteEmployee(empId);
        } catch (EmptyResultDataAccessException e) {
            logger.error("No employee found with ID: " + empId);
            throw new NoSuchEmpException("No such user", ErrorCode.NO_SUCH_EMPLOYEE);
        } catch (Exception e) {
            logger.error("Exception occurred while deleting the employee : " + empId);
            throw e;
        }
    }
}
