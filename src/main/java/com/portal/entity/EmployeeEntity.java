package com.portal.entity;

import com.portal.constants.EmpConstants;

import javax.persistence.*;

@Entity
@Table(name = "EMPLOYEE", schema = EmpConstants.EMP_DB_SCHEMA)
public class EmployeeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_USER_ID")
    @SequenceGenerator(sequenceName = "SQ_USER_ID", allocationSize = 1, name = "SQ_USER_ID")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "AGE", nullable = false)
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
