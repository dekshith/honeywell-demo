package com.portal.model;

import java.util.HashMap;

public class ResponseData {
    private boolean status;
    private String message;
    private Object data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        if (this.message == null) {
            this.message = "";
        }
        this.message = message;
    }

    public Object getData() {
        if (data == null) {
            data = new HashMap();
        }
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}