package com.portal.repository;

import com.portal.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Integer> {

    @Override
    Optional<EmployeeEntity> findById(Integer s);
}
