package com.portal.exception;

public enum ErrorCode {

    NO_SUCH_EMPLOYEE("101",  "No such employee exists"),
    DB_ERROR("11", "Database error"),
    OTHER_ERROR("0", "Other Error")
    ;

    private String errorCode;
    private String errorMessage;
    ErrorCode(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
