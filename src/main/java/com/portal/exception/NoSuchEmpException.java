package com.portal.exception;

public class NoSuchEmpException extends EmpException {

    public NoSuchEmpException(String message, ErrorCode errorCode) {
        super(message, errorCode);
    }
}
