package com.portal.exception;

import com.portal.model.ExceptionResponse;
import com.portal.model.ResponseData;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.*;

@ControllerAdvice
@RestController
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoSuchEmpException.class)
    public final ResponseEntity<ExceptionResponse> handleNoSuchEmpException(Exception ex, WebRequest request) {
        ExceptionResponse error = new ExceptionResponse(new Date(),ex.getMessage(),request.getDescription(false));
        return new ResponseEntity(error, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(EmpException.class)
    public final ResponseEntity<ExceptionResponse> handleEmpException(Exception ex, WebRequest request) {
        ExceptionResponse error = new ExceptionResponse(new Date(),ex.getMessage(),request.getDescription(false));
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseData responseData = new ResponseData();
        responseData.setMessage("Invalid Payload");
        Map<String, String> errorFields = new LinkedHashMap<>();
        List<FieldError> errorList = ex.getBindingResult().getFieldErrors();
        if (errorList != null) {
            for (FieldError error : errorList) {
                errorFields.put(error.getField(), error.getDefaultMessage());
            }
        }
        responseData.setData(errorFields);
        return new ResponseEntity(responseData, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        ExceptionResponse error = new ExceptionResponse(new Date(),"Internal error occurred" ,request.getDescription(false));
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
