package com.portal.exception;

public class EmpException extends Exception {
    private String message;
    private Exception exception;
    private ErrorCode errorCode;

    public EmpException(final String message, final ErrorCode errorCode) {
        super(message);
        this.message = message;
        this.errorCode = errorCode;
    }

    public EmpException(final String message,final Exception exception) {
        super(message, exception);
        this.message = message;
        this.exception = exception;
        this.errorCode = ErrorCode.OTHER_ERROR;
    }

    public EmpException(final String message, final Exception exception, final ErrorCode errorCode) {
        super(message, exception);
        this.message = message;
        this.exception = exception;
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
