package com.portal.dao;

import com.portal.entity.EmployeeEntity;
import com.portal.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeDAO {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Integer addEmployeeData(final EmployeeEntity employeeEntity) {
        Integer employeeId = null;
        EmployeeEntity savedEmpEntity = employeeRepository.save(employeeEntity);
        if (savedEmpEntity != null) {
            employeeId = savedEmpEntity.getId();
        }
        return employeeId;
    }

    public void deleteEmployee(final Integer empId) {
        employeeRepository.deleteById(empId);
    }
}
